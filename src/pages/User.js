import { useState, useContext, useEffect } from 'react';
//bootstrap
import { Container } from 'react-bootstrap';
//components
import AdminViewUsers from '../components/AdminViewUsers';
import UserView from '../components/UserView';

//react context
import UserContext from '../UserContext';

const User = () => {
    const { user } = useContext(UserContext)

    const [ allUser, setAllUser ] = useState([])

    const fetchUserData = () => {
        fetch(`http://localhost:4000/users/getAll`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllUser(data)
        })
    }

    useEffect(() => {
        fetchUserData()
    }, [])

    return (
        <Container>
             {
                (user.isAdmin === true)?
                <AdminViewUsers usersData={allUser} fetchUserData={fetchUserData} />
                :
                <UserView usersData={null} />
            }
        </Container>
    )
}

export default User
