import { Fragment, useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//user context
import UserContext from '../UserContext';
//Routing
import { Redirect, useHistory } from 'react-router-dom';
import '../App.css';

const Login = () => {

    const history = useHistory();

    const { user, setUser } = useContext(UserContext)

    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ isActive, setIsActive ] = useState(false);

    useEffect(() => {
        if((email !== "" && password !== "")){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

    function loginUser(e){
        e.preventDefault();

        fetch(`http://localhost:4000/users/login`, {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.accessToken !== undefined){
                localStorage.setItem('accessToken', data.accessToken)
                setUser({ accessToken: data.accessToken });
                Toast.fire({
                    icon: "success",
                    title: "You have successfully login"
                })
                setEmail("")
                setPassword("")

                fetch(`http://localhost:4000/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data.isAdmin === true){
                        localStorage.setItem('email', data.email)
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({
                            email: data.email,
                            isAdmin: data.isAdmin
                        })

                        history.push('/allusion')
                    }else{
                        history.push('/')
                    }
                })
            }else{
                Toast.fire({
                    icon: "error",
                    title: "Something Went Wrong"
                })
            }
        })
    }


    return (
        (user.accessToken !== null)?
            <Redirect to="/" />
            :
        <Fragment>
            <Form onSubmit={(e) => loginUser(e)} className="mt-4">
                <h1>User Login</h1>
                <Form.Group className="mb-3">
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control 
                    type="email" 
                    placeholder="Enter Email" 
                    value={email} 
                    onChange={e => setEmail(e.target.value)}
                    required
                    />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Enter Password" 
                    value={password} 
                    onChange={e => setPassword(e.target.value)} 
                    required
                    />
                </Form.Group>
                {isActive ? 
                <Button variant="primary" type="submit">Login</Button>
                :
                <Button variant="primary" type="submit" disabled>Login</Button>
                }
            </Form>
            
        </Fragment>
    )
}

export default Login
