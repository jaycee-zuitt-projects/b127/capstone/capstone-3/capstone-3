import { useState, useContext, useEffect } from 'react';
//bootstrap
import { Container } from 'react-bootstrap';
//components
import AdminViewOrders from '../components/AdminViewOrders';
import UserView from '../components/UserView';

//react context
import UserContext from '../UserContext';

const User = () => {
    const { user } = useContext(UserContext)

    const [ allOrders, setAllOrders ] = useState([])

    const fetchAllOrdersData = () => {
        fetch(`http://localhost:4000/orders/allOrders`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllOrders(data)
        })
    }

    useEffect(() => {
        fetchAllOrdersData()
    }, [])

    return (
        <Container>
             {
                (user.isAdmin === true)?
                <AdminViewOrders allOrdersData={allOrders} fetchAllOrdersData={fetchAllOrdersData} />
                :
                <UserView usersData={null} />
            }
        </Container>
    )
}

export default User