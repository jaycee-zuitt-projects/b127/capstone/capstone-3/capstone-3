import {useState, useEffect, useContext} from 'react';
import {Link, useHistory, useParams} from 'react-router-dom';
import {Container, Card, Button, Form, InputGroup} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function SpecificProduct () {
	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [category, setCategory] = useState('');
	const [size, setSize] = useState(0);
	const [color, setColor] = useState(0);


	const [qty, setQty] = useState(1);

	const history = useHistory();

	const incQty = e => {
		if (qty<99) {
			setQty(qty+1)
		}
	}

	const decQty = e => {
		if (qty>=1) {
			setQty(qty-1)
		}
	}
	

	useEffect(() => {
		fetch(`http://localhost:4000/allusion/${productId}`)
		.then(res=>res.json())
		.then(data=> {
			setName(data.name);
			setDescription(data.desc);
			setPrice(data.price);
            setCategory(data.category);
            setSize(data.size);
            setColor(data.color);
		})

	},[])

	const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

	const addToCart = (productId,quantity) => {
		fetch(`http://localhost:4000/cart/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId:productId,
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data=> {
			if (data) {
				Toast.fire({
					icon: 'success',
					title: 'Added to Cart',
				})
			history.push('/allusion')
			} else {
				Toast.fire({
					icon: 'error',
					title: `Something went wrong. Please try again`
				})
			}
		})
	}
	
	return(
		<Container>
			<Card className="mt-4">
				<Card.Header className='bg-dark text-light text center'>
				{name}
				</Card.Header>
				<Card.Body>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Subtitle>Category</Card.Subtitle>
					<Card.Text>{category}</Card.Text>
					<Card.Subtitle>Size</Card.Subtitle>
					<Card.Text>{size}</Card.Text>
					<Card.Subtitle>Color</Card.Subtitle>
					<Card.Text>{color}</Card.Text>
					<Card.Subtitle>Qty:</Card.Subtitle>
					<InputGroup className='w-25 pt-1'>
						<InputGroup.Prepend>
					    	<Button variant="outline-danger" onClick={e=>decQty(e)}>-</Button>
					   	</InputGroup.Prepend>
					   	<Form.Control 
					   		className='text-center w-25' 
					   		value={qty} 
					   		type='text'
					   		maxLength='2' 
					   		onChange={e=>{
					   			if(e.target.value!=='') {
					   				setQty(parseInt(e.target.value))
					   			}
					   			else{
					   				setQty(0)
					   			}
					   		}}
					   	/>
						<InputGroup.Append>
							<Button variant="outline-primary" onClick={e=>incQty(e)}>+</Button>
						</InputGroup.Append>
					</InputGroup>
				</Card.Body>
				<Card.Footer>
				{
					user.accessToken !== null?
						<Button variant='primary' onClick={()=>addToCart(productId,qty)}>Add to Cart</Button>
						:
						<Link className='btn btn-primary' to='/login'>Login to add to cart</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
	)
}
