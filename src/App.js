import { Fragment, useState, useEffect } from 'react';
import './App.css';
// Bootstrap
import { Container } from 'react-bootstrap';
//React Context
import { UserProvider } from './UserContext';
//components
import AppNavbar from './components/AppNavbar';
//routing components
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
//pages
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Users from './pages/User';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import Orders from './pages/Orders'
// import Error from './pages/Error';


function App() {

  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={ {user, setUser, unsetUser} }>
      <Router>
        < AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/allusion" component={Products} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/allusion/:productId" component={SpecificProduct} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/ordersProto" component={Orders} />
            {/* <Route component={Error} /> */}
          </Switch>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
