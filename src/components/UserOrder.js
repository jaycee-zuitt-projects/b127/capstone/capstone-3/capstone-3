import {Fragment} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function UserOrder({orderData}){
	const {_id, name, quantity, totalPerProduct, purchasedOn} = orderData;
	console.log(orderData)

	return(
		<Fragment>
			<div className='d-flex justify-content-center mt-5'>
					<h2>Transaction History</h2>
			</div>
			<Table striped hover className="mt-4">
				<thead className='bg-dark text-light'>
					<tr className='text-center'>
						<td width='20%'>Date Ordered</td>
						<td width='20%'>Name</td>
						<td width='20%'>Quantity</td>
						<td width='20%'>Total</td>
					</tr>
				</thead>
				<tbody className='hover'>
					<tr key={_id} className='text-center'>
						<td width='20%'>{purchasedOn.slice(0,10)}</td>
						<td width='20%'>{name}</td>
						<td width='20%'>{quantity}</td>
						<td width='20%'>{totalPerProduct}</td>
					</tr>
				</tbody>
				<tfoot>
					<td>
						<Button variant='primary' as={Link} to='/'>Go To Homepage</Button>
					
					</td>
				</tfoot>
			</Table>
		</Fragment>
	)
}
