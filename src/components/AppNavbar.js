import { Fragment, useContext } from 'react';
import { Navbar, Nav, Form } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
//React Context
import UserContext from '../UserContext';

export default function AppNavbar(){
    const { user } = useContext(UserContext)

    let rightNav = (user.accessToken !== null)
    ?
    <Fragment>
        <Nav.Link as={NavLink} to="/cart"> MyCart </Nav.Link>
        <Nav.Link as={NavLink} to="/ordersProto"> Checkout </Nav.Link>
        <Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
    </Fragment>
    :
    <Fragment>
        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    </Fragment>

    let rightNavAdmin = (user.accessToken !== null)
    ?
    <Fragment>
        <Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
    </Fragment>
    :
    <Fragment>
        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    </Fragment>


    return(
       (user.isAdmin === true)?
       <Navbar bg="dark" variant="dark" expand="lg">
                <Navbar.Brand as={Link} to="/" id="logo">Allusion Admin Pannel</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Form className="d-flex">
                </Form>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/users">Users</Nav.Link>
                        <Nav.Link as={NavLink} to="/allusion">Products</Nav.Link>
                        {rightNavAdmin}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        :
        <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand as={Link} to="/" id="logo">Allusion Apparel Co.</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Form className="d-flex">
            </Form>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/allusion">Products</Nav.Link>
                    {rightNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}