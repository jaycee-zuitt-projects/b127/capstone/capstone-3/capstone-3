import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
//bootstrap
import { Button, Card, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';



const ProductCard = ({ productProp }) => {

    const { _id, name, desc, price, breakPoint } = productProp

    return (
        <Row>
            <Col xs={12} md={breakPoint}>
                <Card style={{ width: "18rem"}} className="mt-5">
                    <Card.Img variant="top" src="#" />
                    <Card.Header><h2>{name}</h2></Card.Header>
                    <Card.Body>
                        <Card.Text>{desc}</Card.Text>
                        <Card.Text>{price}</Card.Text>
                        <Link className='btn btn-warning' to={`/allusion/${_id}`}>Product Details</Link>
                    </Card.Body>
                </Card>
                
            </Col>
        </Row>
    )
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        desc: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

export default ProductCard
