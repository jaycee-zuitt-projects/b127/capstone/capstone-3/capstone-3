import { Carousel, Button, Container } from 'react-bootstrap'



const Banner = () => {
    return (
        <Carousel className="mt-2 pr-2 pl-2">
            <Carousel.Item>
                <img className="d-block w-100" src="./images/hitbu.jpg" alt="First Slide" style={{
                    maxHeight: 550
                }} />
                <Carousel.Caption>
                    <h3>Allusion Apparel Co.</h3>
                    <p>Allusion Street Style Apparels</p>
                    
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src="./images/ONV.jpg" alt="Second Slide" style={{
                    maxHeight: 550
                }} />
                <Carousel.Caption>
                    <h3>Allusion Apparel Co.</h3>
                    <p>Allusion Street Style Apparels</p>
                 
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src="./images/titoweedy.jpg" alt="Third Slide" style={{
                    maxHeight: 550
                }} />
                <Carousel.Caption>
                    <h3>Allusion Apparel Co.</h3>
                    <p>Allusion Street Style Apparels</p>
                    
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}

export default Banner
