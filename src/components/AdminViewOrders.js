import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form, Nav } from 'react-bootstrap';
import Swal from 'sweetalert2'


const AdminViewUsers = (props) => {
    const { usersData, fetchUserData } = props

    //users
    const [ users, setUsers ] = useState([])
    const [ firstName, setFirstName] = useState("")
    const [ lastName, setLastName ] = useState("")
    const [ email, setEmail ] = useState("")
    const [ address, setAddress ] = useState("")
    const [ mobileNo, setMobileNo] = useState("")
    const [ password, setPassword ] = useState("")

    //State for add user
    const [ showAddUser, setShowAdduser ] = useState(false);
    //modal for add user
    const addClose = () => setShowAdduser(false)
    const addShow = () => setShowAdduser(true)

    // //State for edit user
    // const [ showEdit, setShowEdit ] = useState(false);
    const [ userId, setUserId ] = useState('');


    useEffect(() => {
        const usersArr = usersData.map(user => {
            return(
                <tr key={user._id}>
                    <td>{user._id}</td>
                    <td>{user.email}</td>
                    <td>{user.mobileNo}</td>
                    <td className={user.isAdmin ? "text-success" : "text-muted"}>{user.isAdmin ? "Admin" : "Normal User"}</td>
                    <td>
                        {user.isAdmin ?
                        <Button variant="danger" size="sm" onClick={() => removeAdmin(user._id, user.isAdmin)}>Remove</Button>
                        :
                        <Button variant="success" size="sm" onClick={() => makeAdmin(user._id, user.isAdmin)}>Appoint</Button>
                        }
                    </td>
                </tr>
            )
        })
        setUsers(usersArr)
    }, [usersData])

    //Swall fire
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
    return (
        <Fragment>
            <div className="text-center my-4">
                <h2>Admin User List</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="warning" onClick={addShow}>Add New User</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white text-center">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Mobile Number</th>
                        <th>User Type</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    {users}
                </tbody>
            </Table>
        
        </Fragment>
    )
}

export default AdminViewUsers
