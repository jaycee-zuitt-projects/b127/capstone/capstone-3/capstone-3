import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form, Nav } from 'react-bootstrap';
import Swal from 'sweetalert2'


const AdminViewUsers = (props) => {
    const { usersData, fetchUserData } = props

    //users
    const [ users, setUsers ] = useState([])
    const [ firstName, setFirstName] = useState("")
    const [ lastName, setLastName ] = useState("")
    const [ email, setEmail ] = useState("")
    const [ address, setAddress ] = useState("")
    const [ mobileNo, setMobileNo] = useState("")
    const [ password, setPassword ] = useState("")

    //State for add user
    const [ showAddUser, setShowAdduser ] = useState(false);
    //modal for add user
    const addClose = () => setShowAdduser(false)
    const addShow = () => setShowAdduser(true)

    // //State for edit user
    // const [ showEdit, setShowEdit ] = useState(false);
    const [ userId, setUserId ] = useState('');


    useEffect(() => {
        const usersArr = usersData.map(user => {
            return(
                <tr key={user._id}>
                    <td>{user._id}</td>
                    <td>{user.email}</td>
                    <td>{user.mobileNo}</td>
                    <td className={user.isAdmin ? "text-success" : "text-muted"}>{user.isAdmin ? "Admin" : "Normal User"}</td>
                    <td>
                        {user.isAdmin ?
                        <Button variant="danger" size="sm" onClick={() => removeAdmin(user._id, user.isAdmin)}>Remove</Button>
                        :
                        <Button variant="success" size="sm" onClick={() => makeAdmin(user._id, user.isAdmin)}>Appoint</Button>
                        }
                    </td>
                </tr>
            )
        })
        setUsers(usersArr)
    }, [usersData])

    //Swall fire
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      

    // Add new user
    const addUser = (e) => {
        e.preventDefault()
        fetch(`http://localhost:4000/users/register`,{
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ localStorage.getItem('accessToken') }`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                address: address,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                fetchUserData()
                Toast.fire({
                    icon: 'success',
                    title: 'Added a new user successfully'
                  })
            }else{
                fetchUserData()
                Toast.fire({
                    icon: 'success',
                    title: 'Failed to add new user'
                  })
            }
        })
    }

    //Make admin 
    const makeAdmin = (userId, isAdmin) => {
        fetch(`http://localhost:4000/users/${ userId }/makeAdmin`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                isAdmin: isAdmin
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                fetchUserData()
                Toast.fire({
                    icon: "success",
                    title: 'Appoint a new admin successfully'
                })
            }else{
                fetchUserData()
                Toast.fire({
                    icon: "error",
                    title: 'Failed to appoint a new admin'
                })
            }
        })
    }

    //remove admin 
    const removeAdmin = (userId, isAdmin) => {
        fetch(`http://localhost:4000/users/${ userId }/removeAdmin`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                isAdmin: isAdmin
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                fetchUserData()
                Toast.fire({
                    icon: "success",
                    title: 'Removed an admin successfully'
                })
            }else{
                fetchUserData()
                Toast.fire({
                    icon: "error",
                    title: 'Failed to remove an new admin'
                })
            }
        })
    }

    return (
        <Fragment>
            <div className="text-center my-4">
                <h2>Admin User List</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="warning" onClick={addShow}>Add New User</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white text-center">
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>User Type</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    {users}
                </tbody>
            </Table>
            {/* Add new user */}
            <Modal show={showAddUser} onHide={addClose}>
                <Form onSubmit={e => addUser(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add New User</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Firstname:</Form.Label>
                            <Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Lastname:</Form.Label>
                            <Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email:</Form.Label>
                            <Form.Control type="text" value={email} onChange={e => setEmail(e.target.value)} required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Address:</Form.Label>
                            <Form.Control type="text" value={address} onChange={e => setAddress(e.target.value)} required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Mobile Number:</Form.Label>
                            <Form.Control type="number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="success" type="submit" onClick={addClose}>Create</Button>
                        <Button variant="danger" onClick={addClose}>Close</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Fragment>
    )
}

export default AdminViewUsers
