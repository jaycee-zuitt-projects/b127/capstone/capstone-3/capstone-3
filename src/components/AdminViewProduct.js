import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';

const AdminViewProduct = (props) => {
    const { productData, fetchData } = props

    //products
    const [ products, setProducts ] = useState([])
    const [ name, setName ] = useState("");
    const [ description, setDescription ] = useState("")
    const [ price, setPrice ] = useState("")
    const [ category, setCategory ] = useState('')
    const [ size, setSize ] = useState ('')
    const [ color, setColor ] = useState('')

    //State for addproduct 
    const [ showAdd, setShowAdd ] = useState(false);
    //State for editproduct
    const [ showEdit, setShowEdit ] = useState('')
    const [ productId, setProductId ] = useState('')

    //modal for addproduct
    const addClose = () => setShowAdd(false);
    const addShow = () => setShowAdd(true);

    //modal for edit
    const closeEdit = () => {
        setShowEdit(false)
        setName('')
        setDescription('')
        setPrice(0)
        setCategory('')
        setSize('')
        setColor('')
    }

    const openEdit = (productId) => {
        fetch(`${ process.env.API_URL }/allusion/${ productId }`)
        .then(res => res.json())
        .then(data => {
            setProductId(data._id)
            setName(data.name)
            setDescription(data.desc)
            setPrice(data.price)
            setCategory(data.category)
            setSize(data.size)
            setColor(data.color)
        })
        setShowEdit(true)
    }

    useEffect(() => {
        const productsArr = productData.map(product => {
            return(
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.desc}</td>
                    <td>{product.price}</td>
                    <td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
                    <td>
                        <Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
                        {/* archived product */}
                        {product.isActive ?
                        <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Archived</Button>
                        :
                        <Button variant="success" size="sm" onClick={() => activateToggle(product._id, product.isActive)}>Enable</Button>
                        }
                    </td>
                </tr>
            )
        })
        setProducts(productsArr)
    }, [productData])

    //Swall fire
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

    // Create new product

    const addProduct = (e) => {
        e.preventDefault();
        fetch(`http://localhost:4000/allusion/createProduct`, {
            method: "POST",
            headers:{
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                name: name,
                desc: description,
                price: price,
                category: category,
                size: size,
                color: color
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
                if(data === true){
                    fetchData()
                    Toast.fire({
                        icon: 'success',
                        title: 'Added a new product successfully'
                      })
                    setName('')
                    setDescription('')
                    setPrice(0)
                    setCategory('')
                    setSize('')
                    setColor('')

                    addClose()
                }else{
                    Toast.fire({
                        icon: 'error',
                        title: 'Failed to add product'
                      })
                }
        })
    }

    // edit product

    const editProduct = (e) => {
        e.preventDefault();

        fetch(`http://localhost:4000/allusion/${productId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                name: name,
                desc: description,
                price: price,
                category: category,
                size: size,
                color: color
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                fetchData()
                Toast.fire({
                    icon: 'success',
                    title: 'Update successfully'
                  })
                closeEdit()
            }else{
                fetchData()
                Toast.fire({
                    icon: 'error',
                    title: 'Failed to update'
                  })
            }
        })
    }

    // disable product
    const archiveToggle = (productId, isActive) => {
        fetch(`http://localhost:4000/allusion/${productId}/archive`, {
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                fetchData()
                Toast.fire({
                    icon: 'success',
                    title: 'Archived/disabled successfully.'
                  })
            }else{
                fetchData()
                Toast.fire({
                    icon: 'error',
                    title: 'Failed to archived'
                  })
            }
        })
    }

    const activateToggle = (productId, isActive) => {
        fetch(`http://localhost:4000/allusion/${ productId }/activate`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem("accessToken") }`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === false){
                fetchData()
                Toast.fire({
                    icon: 'success',
                    title: 'Activate/Enabled successfully'
                  })
            }else{
                fetchData()
                Toast.fire({
                    icon: 'error',
                    title: 'Failed to activate'
                  })
            }
        })
    }

    return (
        <Fragment>
            <div className="text-center my-4">
                <h2>Admin Product List</h2>
                <div className='d-flex justify-content-center'>
                    <Button variant='warning' onClick={addShow}>Create New Product</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white text-center">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Available</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>

           {/* New Product Modal */}

            <Modal show={showAdd} onHide={addClose}>
                <Form onSubmit={e => addProduct(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create New Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Categories:</Form.Label>
                            <Form.Control type="array" value={category} onChange={e => setCategory(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Size:</Form.Label>
                            <Form.Control type="array" value={size} onChange={e => setSize(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Color:</Form.Label>
                            <Form.Control type="array" value={color} onChange={e => setColor(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>
                    
                    <Modal.Footer>
                        <Button variant="success" type="submit">Upload Product</Button>
                        <Button variant="danger" onClick={addClose}>Exit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            {/* Edit modal */}
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Categories:</Form.Label>
                            <Form.Control type="array" value={category} onChange={e => setCategory(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Size:</Form.Label>
                            <Form.Control type="array" value={size} onChange={e => setSize(e.target.value)} required />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Color:</Form.Label>
                            <Form.Control type="array" value={color} onChange={e => setColor(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>
                    
                    <Modal.Footer>
                        <Button variant="success" type="submit">Upload Product</Button>
                        <Button variant="danger" onClick={closeEdit}>Exit</Button>
                    </Modal.Footer>

                </Form>
            </Modal>
        </Fragment>
    )
}

export default AdminViewProduct
